import cv2
import numpy as np
from hazmat import Hazmat


class ObjectDetection:
    def __init__(self, config_path, weights_path, labels_path, input_weight=416, input_height=416, min_size_percent=0.1, min_confidence=0.5):
        self._net = cv2.dnn.readNetFromDarknet(config_path, weights_path)

        ln = self._net.getLayerNames()
        self._last_layers = [ln[i[0] - 1]
                             for i in self._net.getUnconnectedOutLayers()]

        self._labels = open(labels_path).read().strip().split("\n")
        self.input_weight = input_weight
        self.input_height = input_height
        self.min_size_percent = min_size_percent
        self.min_confidence = min_confidence

    def output_layers(self, image):
        blob = cv2.dnn.blobFromImage(
            image,
            1 / 255.0,
            (self.input_weight, self.input_height),
            swapRB=True,
            crop=False
        )
        self._net.setInput(blob)
        return self._net.forward(self._last_layers)

    def detect(self, image):
        image_h, image_w = image.shape[:2]
        boxes = []
        confidences = []
        labels = []
        for output in self.output_layers(image):
            for detection in output:
                scores = detection[5:]
                class_id = np.argmax(scores)
                confidence = scores[class_id]
                if confidence < self.min_confidence:
                    continue
                box = detection[0:4]
                if max(box[2:]) < self.min_size_percent:
                    continue
                box = box * np.array([image_w, image_h, image_w, image_h])                    
                (center_x, center_y, w, h) = box

                x = int(center_x - (w / 2))
                y = int(center_y - (h / 2))

                boxes.append([x, y, int(w), int(h)])
                confidences.append(float(confidence))
                labels.append(self._labels[class_id])

        output = []
        idxs = cv2.dnn.NMSBoxes(boxes, confidences, 0.58, 0.3)
        if len(idxs) > 0:
            for i in idxs.flatten():
                (x, y) = (boxes[i][0], boxes[i][1])
                (w, h) = (boxes[i][2], boxes[i][3])
                confidence = confidences[i]
                output.append(Hazmat(x, y, w, h, labels[i], confidence))
        return output
