class Hazmat:
    def __init__(self, x, y, w, h, name, confidence):
        self.x = x
        self.y = y
        self.w = w
        self.h = h
        self.name = name
        self.confidence = confidence

    def __str__(self):
        return 'x: {} y: {} w: {} h: {} name: {} confidence: {}'.format(self.x, self.y, self.w, self.h, self.name, self.confidence)

    def to_float(self, image):
        h, w = image.shape[:2]
        h = float(h)
        w = float(w)
        return self.x / w, self.y / h, self.w / w, self.h / h

    def box(self):
        return self.x, self.y, self.w, self.h