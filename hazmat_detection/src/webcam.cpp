#include <hazmat_detection/neural_network.h>
#include <opencv2/highgui.hpp>

int main(int argc, char **argv)
{
    cv::VideoCapture camera(0);
    NeuralNetwork nn;
    while (cv::waitKey(1) < 0)
    {
        cv::Mat frame;
        camera >> frame;
        if (frame.empty())
            break;
        nn.detect(frame);
        cv::imshow("frame", frame);
    }    
    return 0;
}
